<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Form BPJS</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <style>
      .btn-file {
    position: relative;
    overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 100%;
    }

    /* .value {
  border-bottom: 4px dashed #bdc3c7;
  text-align: center;
  font-weight: bold;
  font-size: 10em;
  width: 300px; 
  height: 100px;
  line-height: 60px;
  margin: 40px auto;
  letter-spacing: -.07em;
  text-shadow: white 2px 2px 2px;
}
input[type="range"] {
  display: block;
  -webkit-appearance: none;
  background-color: #bdc3c7;
  width: 300px;
  height: 5px;
  border-radius: 5px;
  margin: 0 auto;
  outline: 0;
}
input[type="range"]::-webkit-slider-thumb {
  -webkit-appearance: none;
  background-color: #e74c3c;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  border: 2px solid white;
  cursor: pointer;
  transition: .3s ease-in-out;
}​
  input[type="range"]::-webkit-slider-thumb:hover {
    background-color: white;
    border: 2px solid #e74c3c;
  }
  input[type="range"]::-webkit-slider-thumb:active {
    transform: scale(1.6);
  } */

    </style>
  </head>
  <body>
    <div class="container px-4 py-4">
      <div class="row mt-3 mb-3">
        <div class="col text-center">
          <h2>CV Form</h2>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12">
        <h4>Personal Details</h4>
          <br>
        </div>
        <div class="col-md-6 mb-4">
            <label for="exampleFormControlInput1" class="form-label">Wanted Job Title <a class="ml-1" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Input your job title you want"><i class="fa-regular fa-circle-question" style="color:skyblue"></i></a></label>
            <input type="text" class="form-control">
        </div>
        <div class="col-md-1 mb-4">
            <img id='img-upload' src="https://t2fire.co.uk/wp-content/uploads/2019/11/default-avatar-250x250.png.webp" alt="" style="width: 75px;height: 75px" class="mr-2">
            <!-- <span><input type="file"></span> -->
        </div>
        <div class="col-md-5">
        <div class="form-group">
        <label>Upload Photo</label>
        <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-outline-secondary btn-file">
                    Browse… <input type="file" id="imgInp" accept="image/jpeg, image/png">
                </span>
            </span>
            <input type="text" class="form-control" readonly>
        </div>
        <!-- <img id='img-upload'/> -->
    </div>
        </div>
        <div class="col-md-6 mb-4">
          <label for="">First Name</label>
          <input type="text" class="form-control">
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Last Name</label>
          <input type="text" class="form-control">
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Email</label>
          <input type="email" class="form-control">
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Phone</label>
          <input type="number" class="form-control">
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Country</label>
          <select name="" id="" class="form-control">
            <option value=""> -- Select Country --</option>
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
          </select>
        </div>
        <div class="col-md-6 mb-4">
          <label for="">City</label>
          <select name="" id="" class="form-control">
            <option value=""> -- Select City --</option>
            <option value="jakarta">Jakarta</option>
            <option value="tangerang">Tangerang</option>
          </select>
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Address</label>
          <textarea class="form-control"></textarea>
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Postal Code</label>
          <input type="number" class="form-control">
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Driving License <a class="ml-1" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Input your driver license"><i class="fa-regular fa-circle-question" style="color:skyblue"></i></a></label>
          <input type="number" class="form-control">
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Nationality <a class="ml-1" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Input your nationality"><i class="fa-regular fa-circle-question" style="color:skyblue"></i></a></label>
          <input type="text" class="form-control">
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Place Of Birth</label>
          <input type="text" class="form-control">
        </div>
        <div class="col-md-6 mb-4">
          <label for="">Date Of Birth</label>
          <input type="date" class="form-control">
        </div>
      </div>

    <div class="row mt-5">
      <div class="col-12">
        <h4>Professional Summary</h4>
        <p>Write 2-4 short & energetic sentences to interest the reader! Mention your role, experience & most importantly - your biggest achievements best gualities and stalls.  </p>
      </div>
      <div class="col-md-12">
      <div id="summernote"></div>
      </div>
    </div>

    <div class="row mt-5">
      <div class="col-12">
        <h4>Employment History</h4>
        <p>Show your relevant experience (last 10 years). Use bullet points to note your achievement, if possible - use numbers/facts (Achieved X, measured by Y, by doing Z).</p>
        <div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <p id="head-accordion">(Not specified)</p>
          <p id="head-accordion-date"></p>
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="">Job title</label>
            <input type="text" class="form-control" id="jt1">
          </div>
          <div class="col-md-6 mb-3">
            <label for="">Employer</label>
            <input type="text" class="form-control" id="em1">
          </div>
          <div class="col-md-6 mb-3">
            <label for="">Start & End Date <a class="ml-1" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Input your nationality"><i class="fa-regular fa-circle-question" style="color:skyblue"></i></a></label>
            <div class="row">
            <div class="col-6">
              <!-- <input type="date" class="form-control"> -->
              <input type="month" class="form-control" id="start-date">
            </div>
            <div class="col-6">
              <input type="month" class="form-control" id="end-date">
            </div>
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="">City</label>
            <input type="text" class="form-control">
          </div>
          <div class="col mb-3">
            <label for="">Description</label>
              <div id="summernote_emp"></div>
          </div>
        </div>
        <a role="button" onclick="myFunction()">+ Add one more employement</a>
      </div>
    </div>
  </div>
</div>
      </div>
    </div>

    <div class="row mt-5">
      <div class="col-12">
        <h4>Education</h4>
        <p>A varied education on your resume sums up the value that your learnings and background will bring to job.</p>
        <div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <p id="head-accordion-ed">(Not specified)</p>
          <p id="head-accordion-date-ed"></p>
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="">School</label>
            <input type="text" class="form-control" id="school">
          </div>
          <div class="col-md-6 mb-3">
            <label for="">Degree</label>
            <input type="text" class="form-control" id="degree">
          </div>
          <div class="col-md-6 mb-3">
            <label for="">Start & End Date <a class="ml-1" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Input your nationality"><i class="fa-regular fa-circle-question" style="color:skyblue"></i></a></label>
            <div class="row">
            <div class="col-6">
              <!-- <input type="date" class="form-control"> -->
              <input type="month" class="form-control" id="start-date-ed">
            </div>
            <div class="col-6">
              <input type="month" class="form-control" id="end-date-ed">
            </div>
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="">City</label>
            <input type="text" class="form-control">
          </div>
          <div class="col mb-3">
            <label for="">Description</label>
              <div id="summernote_ed"></div>
          </div>
        </div>
        <a role="button" onclick="myFunction2()">+ Add one more education</a>
      </div>
    </div>
  </div>
</div>
      </div>
    </div>

    <div class="row mt-5 mb-5">
      <div class="col-12">
        <h4>Skills</h4>
        <p>Choose 5 of the most important skills to show your talents! Make sure they match the keywords of the job listing if applying via an online system.</p>
        <p>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="Java" id="btn-skill">
          Java +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="JavaScript" id="btn-skill">
          JavaScript +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="Phyton" id="btn-skill">
          Phyton +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="Git" id="btn-skill"> 
          Git +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="Sql" id="btn-skill">
          Sql +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="C++" id="btn-skill">
          C++ +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="Typescript" id="btn-skill">
          Typescript +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="C#" id="btn-skill">
          C# +
        </a>
      </p>
      <p>
      <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="Docker" id="btn-skill">
          Docker +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="PHP" id="btn-skill">
          PHP +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="React" id="btn-skill">
          React +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="MongoDB" id="btn-skill">
          MongoDB +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="Toad" id="btn-skill">
          Toad +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="HTML CSS 3" id="btn-skill">
          HTML CSS 3 +
        </a>
        <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" data-skill="MS SQL server" id="btn-skill">
          MS SQL server +
        </a>
      </p>

      <div id="skill-append">

      </div>

      <div class="collapse" id="collapseExample">
        <div class="card card-body">
          <b id="skill"></b>
          <br>
          <div class="row mt-3 mb-3">
            <div class="col-md-6">
              <label for="">Skill</label>
              <input type="text" class="form-control" id="skill-input" disabled>
            </div>
            <div class="col-md-6">
              <label for="">Level - <span id="level-text" style="color:red">Beginner</span></label>
            <!-- <div class="value">0</div> -->
            <input type="range" min="1" max="5" value="0" step="1" class="form-control-range mt-2" onchange="updateLevel(this.value);">
            </div>
          </div>
        </div>
        <button class="btn btn-light mt-2" onclick="addSkills(this.value)" id="addskill">Add one more skill +</button>
      </div>
      </div>
    </div>

    </div>
    <br>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script>
    
    // POPOVER
    $(function (){
      $('[data-toggle="popover"]').popover({
        trigger: 'focus'
      });
    });

    // IMAGE UPLOAD
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		}); 	
	});

  // SUMMERNOTE
  $('#summernote').summernote({
        placeholder: 'e.g Passionate science teacher with 8+ years of experience and a track record of ...',
        tabsize: 2,
        height: 200
    });

    $('#summernote_emp').summernote({
        placeholder: 'e.g. Created and implemented lesson plans based on child-led interests and curiosities.',
        tabsize: 2,
        height: 200
    });

    $('#summernote_ed').summernote({
        placeholder: 'e.g. Graduated with High Honors.',
        tabsize: 2,
        height: 200
    });

    // EMP HISTORY
    function myFunction() {
      if (document.getElementById("jt1").value != '' || document.getElementById("em1").value != '' || document.getElementById("start-date").value != '' || document.getElementById("end-date").value != '') {
        
      
      const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
      ];

      $('#head-accordion').html(document.getElementById("jt1").value+' at '+document.getElementById("em1").value);
      $('#head-accordion-date').html(monthNames[new Date(document.getElementById("start-date").value).getMonth()]+' '+new Date(document.getElementById("start-date").value).getFullYear()+' - '+monthNames[new Date(document.getElementById("end-date").value).getMonth()]+' '+new Date(document.getElementById("end-date").value).getFullYear());
    }
    }

    // ED HISTORY
    function myFunction2() {
      if (document.getElementById("school").value != '' || document.getElementById("degree").value != '' || document.getElementById("start-date-ed").value != '' || document.getElementById("end-date-ed").value != '') {
        
      
      const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
      ];

      $('#head-accordion-ed').html(document.getElementById("school").value+' at '+document.getElementById("degree").value);
      $('#head-accordion-date-ed').html(monthNames[new Date(document.getElementById("start-date-ed").value).getMonth()]+' '+new Date(document.getElementById("start-date-ed").value).getFullYear()+' - '+monthNames[new Date(document.getElementById("end-date-ed").value).getMonth()]+' '+new Date(document.getElementById("end-date-ed").value).getFullYear());
    }
    }

    // SKILLs
    $(document).on('click','#btn-skill',function(){
      var skill = $(this).data("skill");
      $('#skill').html(skill);
      $('#skill-input').val(skill);
      $('#addskill').val(skill);
    });

    function updateLevel(val) {
          if (val == 0) {
            $('#level-text').html('')
          } else if ( val == 1) {
            $('#level-text').html('Beginner').css("color", "red")
          } else if ( val == 2) {
            $('#level-text').html('Trainee').css("color", "#FD841F")
          } else if ( val == 3) {
            $('#level-text').html('Skillfull').css("color", "#E14D2A")
          } else if ( val == 4) {
            $('#level-text').html('Experienced').css("color", "green")
          } else if ( val == 5) {
            $('#level-text').html('Expert').css("color", "#8D9EFF")
          }
        }
    
  // APPEND SKILL
  var i = 1
  // $(document).on('click','#addskill',function(){
  //   ++i;

    // var skill = $(this).data("skill");
    //   $('#skill').html(skill);
    //   $('#skill-input').val(skill);

    //   $("#skill-append").append(`<div class="card card-body">
    //                                 <b id="skill`+i+`"></b>
    //                                 <br>
    //                                 <div class="row mt-3 mb-3">
    //                                   <div class="col-md-6">
    //                                     <label for="">Skill</label>
    //                                     <input type="text" class="form-control" id="skill-input`+i+`" disabled>
    //                                   </div>
    //                                   <div class="col-md-6">
    //                                     <label for="">Level - <span id="level-text" style="color:red">Beginner</span></label>
    //                                   <!-- <div class="value">0</div> -->
    //                                   // <input type="range" min="1" max="5" value="0" step="1" class="form-control-range mt-2" onchange="updateLevel`+i+`(this.value);">
    //                                   </div>
    //                                 </div>
    //                               </div>`)
    // });

    function addSkills(val) {
      $(`a[data-skill="${val}"]`).prop('disabled', true).css("background-color", "blue").css("color", "white");
      $("#skill-append").append(`<div class="card card-body">
                                    <b>`+val+`</b>
                                    <br>
                                    <div class="row mt-3 mb-3">
                                      <div class="col-md-6">
                                        <label for="">Skill</label>
                                        <input type="text" class="form-control" value="`+val+`" disabled>
                                      </div>
                                      <div class="col-md-6">
                                        <label for="">Level - <span id="level-text" style="color:red">Beginner</span></label>
                                      <!-- <div class="value">0</div> -->
                                      <input type="range" min="1" max="5" value="0" step="1" class="form-control-range mt-2" onchange="updateLevel(this.value);">
                                      </div>
                                    </div>
                                  </div>`)
    }
  
  </script>
    </body>
</html>